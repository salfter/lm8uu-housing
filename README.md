(mirrored from [Thingiverse](https://www.thingiverse.com/thing:2640562))

LM8UU housing
=============

In the objective to shave some mass on the ANET's Y and X carriages, here is
a standard sized LM8UU holder or housing.

It is designed to exactly fit the LM8UU linear bearing or any plastic one. 
As it is parametric, you can customize it (open in customizer button at the
bottom right of the image) in order to adapt it to your bearing and to the
quality of your printer ;-)

Everything included, more than 20 grams per housing are won.  Which means 84
grams recovered only for the housings of the Y carriage.  If you change the
LM8UU by those printed ones (highly advised !  see here:
https://www.thingiverse.com/thing:2537701) 50 other grams are shaved ...

==> total gain of 134 grams... not too bad !

You will need small pins in order to lock the bearing in their housing
(small holes are printed to insert the pin right in place, see the blue
arrows in the picture of the X carriage).

My goal was to make those housing so that in case of wearing of the bushings
they could be changed, on the ANET A8, without having to remove the bed and
maybe, even without having to level the bed !  Just push the Y rod accross
the frame, remove the pin, push the old bushing from behind and pull it ! 
With the spiral vase bushings, as they are springy, they will quit the
housing very easily...

Last word, the original M4 screws are just too short for this housing,
you'll have to procure 10 mm long ones.

